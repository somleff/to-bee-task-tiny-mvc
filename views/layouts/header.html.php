<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="/public/css/style.css">
    <title><?= $page_title; ?></title>
</head>
<body>

    <nav class="navbar navbar-expand-lg navbar-light bg-light py-0 my-0">
        <div class="container">
        <a class="navbar-brand mb-0 h1" href="/">To Bee Task</a>

        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link text-info" href="/create">Create Task</a>
                </li>
            </ul>


            <a class="nav-link navbar-right" href="/login">
                <button class="btn btn-light text-info" type="submit">
                    Login
                </button>
            </a>
        </div>
    </div>
    </nav>
