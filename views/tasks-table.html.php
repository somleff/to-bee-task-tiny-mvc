<?php require ('layouts/header.html.php'); ?>

<div class="container mt-4">
    <div class="row card">
        <div class="card-header">
            All Task To Bee

            <table class="table table-hover text-center">

                <thead>
                <tr>
                    <th scope="col"> ID </th>
                    <th scope="col"> Task </th>
                    <th scope="col">
                        <a href="/?sort=name_asc">&#8593;</a> 
                            Name 
                        <a href="/?sort=name_desc">&#8595;</a>
                    </th>
                    <th scope="col">
                        <a href="/?sort=email_asc">&#8593;</a>
                            Email 
                        <a href="/?sort=email_desc">&#8595;</a>
                    </th>
                    <th scope="col">
                        <a href="/?sort=status_asc">&#8593;</a>
                            Status
                        <a href="/?sort=status_desc">&#8595;</a>
                    </th>
                    <th scope="col">Image</th>
                </tr>
                </thead>

                <tbody class="text-center">
                    <?php foreach ($tasks as $task) : ?>
                    <tr>
                        <td scope="row"><?= $task->id; ?></td>
                        <td><?= $task->task; ?></td>
                        <td><?= $task->name; ?></td>
                        <td><?= $task->email; ?></td>
                        <td>
                            <?php if ($task->status) : ?>
                        <span class="icon">&#9989;</span>
                            <?php else : ?>
                        <span class="icon">&#10060;</span>
                            <?php endif?>
                        </td>
                        <td>
                            <img src="<?= $task->location; ?>" width="80" heidth="60">
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>

            </table>

        </div>
    </div>
</div>

 <?php include ('elements/pagination.html.php'); ?>

<?php require ('layouts/footer.html.php'); ?>
