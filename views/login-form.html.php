<?php require ('layouts/header.html.php'); ?>

<div class="container mt-4">
    <div class="row card">
    <div class="card-header">
        Login Admin
    </div>
    <form class="card-body col-md-8 col-md-offset-2 text-center mx-auto" action="/login" method="POST">

        <div class="form-group row">
            <label for="login" class="col-sm-2 col-form-label">Login</label>
            <div class="col-sm-10">
              <input type="login" class="form-control" id="login" placeholder="Enter Your Login">
            </div>
        </div>

        <div class="form-group row">
            <label for="password" class="col-sm-2 col-form-label">Password</label>
            <div class="col-sm-10">
                <input type="password" class="form-control" id="password" placeholder="Enter Your Pass">
            </div>
        </div>

        <div class="form-group row text-center">
            <div class="col-sm-12">
                <button type="submit" class="btn btn-outline-info">Get In</button>
                <a class="btn btn-outline-secondary" href="../">Home</a>
            </div>
        </div>

    </form>
</div>
</div>

<?php require ('layouts/footer.html.php'); ?>
