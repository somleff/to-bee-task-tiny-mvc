<?php require ('layouts/header.html.php'); ?>

<div class="container mt-4">

    <div class="row card">
    <div class="card-header">
        Create Your Task
    </div>
  <form class="card-body col-lg-8 col-md-offset-2 text-center mx-auto" action="/publish" method="POST" enctype="multipart/form-data">

    <div class="form-group row">
        <label for="name" class="col-lg-2 col-form-label">Name</label>
        <div class="col-lg-10">
          <input name="name" id="fname" class="form-control" placeholder="Enter Your Name">
        </div>
    </div>

    <div class="form-group row">
        <label for="email" class="col-lg-2 col-form-label">Email</label>
        <div class="col-lg-10">
            <input name="email" id="femail" class="form-control" placeholder="Enter Your Email">
        </div>
    </div>

    <div class="form-group row">
        <label for="task" class="col-lg-2 col-form-label">Task</label>
        <div class="col-lg-10">
            <input name="task" id="ftask" class="form-control" placeholder="Write Your Task">
        </div>
    </div>

    <div class="form-group row">
        <label for="status" class="col-lg-2 col-form-label">Status</label>
    <div class="col-lg-10">
        <select name="status" id="fstatus" class="form-control">
          <option>0</option>
          <option>1</option>
        </select>
    </div>
    </div>

    <div class="form-group row">
        <label for="upload" class="col-lg-2 col-form-label">Upload</label>
        <div class="col-lg-10">
        <div class="custom-file">
                <input type="image" name="upload" id="upload" class="custom-file-input">
                <input type="hidden" name="action" value="upload">
                <label class="custom-file-label" for="upload"></label>
            </div>
        </div>
    </div>

    <div class="form-group row text-center">
      <div class="col-lg-12">
        <?php include ('elements/module.html.php'); ?>
        <!-- <button type="submit" class="btn btn-outline-info">Publish</button> -->
        <a class="btn btn-outline-secondary" href="../">Home</a>
      </div>
    </div>

  </form>
</div>
</div>

<?php require ('layouts/footer.html.php'); ?>
