<!-- Button trigger modal -->
<button type="button" class="btn btn-outline-info" data-toggle="modal" data-target="#exampleModal" id="previewBtn">
  Publish
</button>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Task Preview</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Are you sure you want to publish the following details?
        <table class="table">
            <tr>
                <th>Name</th>
                <td id="name"></td>
            </tr>
            <tr>
                <th>Email</th>
                <td id="email"></td>
            </tr>
            <tr>
                <th>Task</th>
                <td id="task"></td>
            </tr>
            <tr>
                <th>Status</th>
                <td id="status"></td>
            </tr>
            <tr>
                <th>Image</th>
                <td id="image"><img class="thumbnail img-preview" src="http://farm4.static.flickr.com/3316/3546531954_eef60a3d37.jpg" title="Preview Logo" width="80" heidth="60"></td>
            </tr>
        </table>
            </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" id="submit" class="btn btn-info">Publish</button>
      </div>
    </div>
  </div>
</div>
