<div class="container mt-4">
    <?php if ($total_pages > 1) :?>
        <ul class="pagination justify-content-center">
            <li class="page-item">
              <a class="page-link" href="/?page=<?php ($page>1 ? print($page-1) : print 1)?>&sort=<?= $sort; ?>" tabindex="-1">Previous</a>
            </li>
            <?php for ($i=1; $i<=$total_pages; $i++) :?>
            <li class="page-item"><a class="page-link" href="/?page=<?= $i; ?>&sort=<?= $sort; ?>"><?= $i; ?></a></li>
            <?php endfor;?>
            <li class="page-item">
              <a class="page-link" href="/?page=<?php ($page < $total_pages ? print($page+1) : print $total_pages)?>&sort=<?= $sort; ?>">Next</a>
            </li>
        </ul>
    <?php endif; ?>
</div>
