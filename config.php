<?php

return [
    'database' => [
        'host' => 'mysql:host=127.0.0.1',
        'dbname' => 'dbname=tobee',
        'username' => 'root',
        'password' => '',
        'options' => [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING
        ]
    ]
];
