<?php
//  DB configuration.
App::bind('config', require 'config.php');

//  Making connection to DB 
//  and initiated of Query Builder
//  to make sql queries.
App::bind('database', new QueryBuilder(
    Connection::make(App::get('config')['database'])
));

//  Helper for display views file.
function view($name, $data = [])
{
    extract($data);

    return require "views/{$name}.html.php";
}
