<?php

class Request
{
    //  Fetching information 
    //  about current browser request.
    public static function uri()
    {
        return trim(
            parse_url ($_SERVER['REQUEST_URI'], PHP_URL_PATH), '/'
        );
    }

    //  Figure out request type.
    public static function method()
    {
        return $_SERVER['REQUEST_METHOD'];
    }
}
