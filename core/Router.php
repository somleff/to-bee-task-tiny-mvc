<?php

class Router
{
    //  Explicit request type.
    public $routes = [
        'GET' => [],
        'POST' => []
    ];

    /**
     * Recive file about routes.
     *
     * @param  php doc  $file
     * @return instance
     */
    public static function load($file)
    {
        $router = new Router;
        require $file;
        return $router;
    }

    /**
     * Registered routes specifically for get and post request.
     *
     * @param  string key  $uri
     * @param  string  $controller
     * @return string
     */
    public function get($uri, $controller)
    {
        $this->routes['GET'][$uri] = $controller;
    }

            public function post($uri, $controller)
            {
                $this->routes['POST'][$uri] = $controller;
            }

    /**
     * Get uri  and require the controller associated with it.
     * Return the route what user require.
     *
     * @param  string  $uri
     * @param  string  $requestType
     * @return string  
     */
    public function direct($uri, $requestType)
    {
        if (array_key_exists($uri, $this->routes[$requestType])) {   
            return $this->callMethod(
                //  Pass arguments to the method
                ...explode('@', $this->routes[$requestType][$uri])
            );
        }

        throw new Exception('No route defined for this URI.');
    }

    /**
     * 
     * Сreate the controller object and get the method with the response to the query.
     *
     * @param  object  $controller
     * @param  method  $method
     * @return method  
     */
    protected function callMethod($controller, $method)
    {
        $controller = new $controller;

        if (! method_exists($controller, $method)) {
            throw new Exception(
                "{$controller} does not respond to the {$method} action"
            );
        }

        return $controller->$method();
    }
}
