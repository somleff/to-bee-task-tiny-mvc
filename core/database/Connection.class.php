<?php

class Connection
{
     /**
     * Make connection to DB.
     *
     * @param  array  $config
     * @return object
     */
    public static function make($config)
    {
        try {
            return new PDO(
                $config['host'] .';'. $config['dbname'],
                $config['username'],
                $config['password'],
                $config['options']
            );
        } catch (PDOException $e) {
            die ($e->getMessage());
        }
    }
}
