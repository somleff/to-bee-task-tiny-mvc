<?php

class QueryBuilder
{
    //  Passed access to PDO Object.
    protected $pdo;

    public function __construct(PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    /**
     * Receive all data from DB.
     *
     * @param string  $table | Name of DB's table.
     * @param string $table2 | Name of 2nd DB's table.
     * @return class
     */
    public function selectAll($table, $table2)
    {
        $statement = $this->pdo->prepare(
            "SELECT {$table}.*, {$table2}.location 
             FROM {$table} JOIN {$table2}
             ON {$table}.id = {$table2}.id"
         );
        $statement->execute();

        return $statement->fetchAll(PDO::FETCH_CLASS);
    }


    public function count($table)
    {
        $statement = $this->pdo->prepare("SELECT COUNT(*) FROM {$table}");
        $statement->execute();

        return $statement->fetchColumn();
    }

    /**
     * Receive all data from DB.
     *
     * @param string  $table | Name of DB's table.
     * @param string $table2 | Name of 2nd DB's table.
     * @param string $orderBy | Comes from Task::sort().
     * @param int $start | Depend of page number.
     * @param int $limit | Comes from Task::sort().
     * @return class
     */
    public function order($table, $table2, $orderBy, $start, $limit)
    {
        $statement = $this->pdo->prepare(
            "SELECT {$table}.*, {$table2}.location FROM {$table} JOIN {$table2}
             ON {$table}.id = {$table2}.id
             ORDER BY {$orderBy} LIMIT {$start}, {$limit}"
        );
        $statement->execute();

        return $statement->fetchAll(PDO::FETCH_CLASS);
    }


    // // public function sort($table, $start, $limit)
    // // {
    // //     $statement = $this->pdo->prepare(
    // //         "SELECT * FROM $table
    // //          LIMIT $start, $limit"
    // //     );

    // //     var_dump($statement);

    // //     $sort = (isset($_GET['sort'])) ? $_GET['sort'] : 'id';

    // //     switch ($sort) {
    // //         case 'id_asc':
    // //         // case 'task_asc':
    // //         // case 'name_asc':
    // //         // case 'email_asc':
    // //         // case 'status_asc':
    // //             $s = sort((array) $statement);
    // //             break;
    // //         case 'id_desc':
    // //         // case 'task_desc':
    // //         // case 'name_desc':
    // //         // case 'email_desc':
    // //         // case 'status_desc':
    // //             $statement = rsort((array) $statement);
    // //         default:
    // //             $statement = sort($statement);
    // //             $sort = 'id';
    // //             break;
    // //     }
    // //     $statement->execute();

    // //     return array ($statement->fetchAll(PDO::FETCH_CLASS), $sort);
    // // }


    public function publishTask($table)
    {
        if (isset($_POST['action']) && $_POST['action'] == 'upload') {
            $location = 'public/jpg/';
            $name = $_FILES['upload']['name'];
            $size = $_FILES['upload']['size'];
            $type = $_FILES['upload']['type'];
            $tmp_name = $_FILES['upload']['tmp_name'];
            $task = $_POST['task'];
            $user = $_POST['name'];
            $email = $_POST['email'];
            $status = $_POST['status'];
   
            if (move_uploaded_file($tmp_name, $location.$name)) {
                $sql = "INSERT INTO $table (task, name, email, status)
                        VALUES ('$task', '$user', '$email', '$status');";
                $sql .= "INSERT INTO filestore (filename, size, type, location) 
                    VALUES ('$name', '$size', '$type', '$location$name')";
            
                try {
                $statement = $this->pdo->prepare($sql);
                var_dump($sql);
                $statement->execute();
                } catch (Exception $e) {
                die('Whoops, something goes wrong.');
                }
            }            
        }
    }
}


// $sql = sprintf(
            //     'INSERT INTO %s (%s) values (%s)',
            //     $table,
            //     implode(',', array_keys($parameters)),
            //     ':' .implode(', :', array_keys($parameters))
            // );
