<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require 'vendor/autoload.php';
require 'core/bootstrap.php';

//  Make up new Router, load any routes,
//  direct the traffic
//  with information about uri and type of request.
Router::load('routes.php')
    ->direct(Request::uri(), Request::method());
