<?php

class Task
{
    public $page;   // Default number to start shown pagination panel.
    public $limit;  // Number of records to show per page.
    public $start;  // Определяем с какой колонки выводить записи.

    public function __construct($page, $limit, $start)
    {
        $this->page = $page;
        $this->limit = $limit;
        $this->start = $start;
    }


    public function paginate()
    {
        // Count the number of records.       
        $total_rows = App::get('database')->count('tasks');
        // Calcualte the number of pages.
        $total_pages = ceil($total_rows / $this->limit);

        // Determine where in the DB to start returning results.
        if (isset($_GET['page']) && $_GET['page'] != "") {
            $this->page = $_GET['page'];
            $this->start = $this->limit * ($this->page-1);
        }

        return array($total_pages, $this->page);
    }


    public function sort()
    {
        // Determine the sort.
        // Default is by id.
        $sort = (isset($_GET['sort'])) ? $_GET['sort'] : 'id';

        // Determine the sorting order.
        switch ($sort) {
            case 'name_asc':
                $orderBy = 'name ASC';
                break;
            case 'name_desc':
                $orderBy = 'name DESC';
                break;

            case 'email_asc':
                $orderBy = 'email ASC';
                break;
            case 'email_desc':
                $orderBy = 'email DESC';
                break;

            case 'status_asc':
                $orderBy = 'status ASC';
                break;
            case 'status_desc':
                $orderBy = 'status DESC';
                break;
            default:
                $orderBy = 'id DESC';
                $sort = 'id';
                break;
        }
               
        // Define the query.
        $tasks = App::get('database')->order(
            'tasks', 'filestore', $orderBy, $this->start, $this->limit
        );

        return array($tasks, $sort);
    }
}
