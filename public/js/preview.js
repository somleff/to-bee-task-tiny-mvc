$('#previewBtn').click(function() {
     $('#name').text($('#fname').val());
     $('#email').text($('#femail').val());
     $('#task').text($('#ftask').val());
     $('#status').text($('#fstatus').val());
     $('input:image').text($('#upload').val());
});

$(document).ready(function() {
    var brand = document.getElementById('upload');
    brand.className = 'attachment_upload';
    brand.onchange = function() {
        document.getElementById('fakeUploadLogo').value = this.value.substring(12);
    };

    // Source: http://stackoverflow.com/a/4459419/6396981
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function(e) {
                $('.img-preview').attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#logo-id").change(function() {
        readURL(this);
    });
});
