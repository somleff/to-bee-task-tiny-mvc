<?php

class PublishController
{
    public function publish()
    {
        App::get('database')->publishTask('tasks');

        header ('Location: /'); 
    }
}

