<?php

class PagesController
{
    public function home()
    {
        $task = new Task(1, 3, 0);

        [$total_pages, $page] = $task->paginate();
        [$tasks, $sort] = $task->sort();
        
        $page_title = 'To Bee Task';

        return view('tasks-table', compact(
            'total_pages', 'page', 'tasks', 'sort', 'page_title'
        ));
    }


    public function create()
    {
        $page_title = 'Create a Task';

        return view('create-task-form', ['page_title' => $page_title]);
    }


    public function login()
    {
        $page_title = 'Login Admin';

        return view('login-form', compact('page_title'));
    }
}
