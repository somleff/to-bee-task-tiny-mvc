<?php 

$paginator = App::get('database')->paginate('tasks');
[$total_pages, $page, $start, $limit] = $paginator;

/**
 * If you need use paginateTask separately from sortTask,
 * just uncomment line below and remove line above.
 *
 */
// [$tasks, $total_pages, $page, $start, $limit] = $paginator;

$sortirator = App::get('database')->sort('tasks', $start, $limit);
[$tasks, $sort] = $sortirator;

$page_title = 'To Bee Task';
header ('Content-disposition: filename');
// header('Content-type: image/jpeg; charset=utf-8');


require 'views/tasks-table.html.php';
